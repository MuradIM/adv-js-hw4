const tableElement = document.querySelector("tbody")

class Tr {
    constructor({name, episodeId, openingCrawl, characters}) {
        this.episodeId = episodeId
        this.name = name
        this.openingCrawl = openingCrawl
        this.characters = characters
    }

    renderTr() {
        const tr = document.createElement("tr")
        tr.dataset.id = this.episodeId
        tr.innerHTML = `
        <td>${this.toRoman(this.episodeId)}</td>
        <td>${this.name}</td>
        <td>${this.openingCrawl}</td>
        <td><ul id="episode${this.episodeId}"></ul></td>
        `
        return tr
    }

    renderCharacters() {
        this.characters.map(i => {
            const place = document.querySelector(`#episode${this.episodeId}`)

            fetch(i)
                .then(responce => responce.json())
                .then(data => {
                    place.innerHTML += `<li>${data.name.toString()}</li>`
                })
        })
    }

    toRoman(value) {
        switch (value.toString()) {
            case "1":
                value = "I"
                break
            case "2":
                value = "II"
                break
            case "3":
                value = "III"
                break
            case "4":
                value = "IV"
                break
            case "5":
                value = "V"
                break
            case "6":
                value = "VI"
                break
        }
        return value
    }
}

fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    .then(data => {
        data.map(i => {
            const partFilm = new Tr(i)

            partFilm.renderTr()
            tableElement.append(partFilm.renderTr())
            partFilm.renderCharacters()
        })
    })
